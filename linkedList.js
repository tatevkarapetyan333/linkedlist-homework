function LinkedList() {
    this.head = null;
    this.tail = null;
}
LinkedList.prototype.addElementInFront = function(value) {
    let newNode = new Node(value);
    if(this.head == null) {
        this.head = newNode;
        this.tail = newNode;
    }else {
        this.head.prev = newNode;
        newNode.next = this.head;
        this.head = newNode;
    }
    return newNode;

}


LinkedList.prototype.addElement = function(value) {
    let newNode = new Node(value);
    if(this.head == null) {
        this.head = null;
        this.tail = null;
    }else {
        newNode.prev = this.tail;
        this.tail.next = newNode;
        this.tail = newNode;
    }

}


LinkedList.prototype.removeElement = function() {
    if(this.head == null) return null;

    let prevNode = this.tail.prev;
   if(prevNode) {
       prevNode.next = null;
       this.tail = prevNode;
   }else {
       this.head = null;
       this.tail = null;
   }
        
   
}

